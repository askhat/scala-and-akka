package com.lightbend.akka.sample

import akka.actor.{Actor, ActorSystem, Props}

object Main {

  class StartStopActor1 extends Actor {

    override def preStart(): Unit = {
      println("first started")
      context.actorOf(Props[StartStopActor2], "second")
    }
    override def postStop(): Unit = println("first stopped")

    override def receive: Receive = {
      case "stop" => context.stop(self)
    }
  }

  class StartStopActor2 extends Actor {
    override def preStart(): Unit = println("second started")

    override def postStop(): Unit = println("second stopped")

    // Actor.emptyBehavior is a useful placeholder when we don't
    // want to handle any messages in the actor.
    override def receive: Receive = Actor.emptyBehavior
  }



  class SupervisingActor extends Actor {
    val child = context.actorOf(Props[SupervisedActor], "supervised-actor")

    override def receive: Receive = {
      case "failChild" => child ! "fail"
    }
  }

  class SupervisedActor extends Actor {
    override def preStart(): Unit = println("supervised actor started")
    override def postStop(): Unit = println("supervised actor stopped")

    override def receive: Receive = {
      case "fail" =>
        println("supervised actor fails now")
        throw new Exception("I failed!")
    }
  }



  def main(args: Array[String]): Unit = {
    val system = ActorSystem("SimpleSystem")

    val supervisingActor = system.actorOf(Props[SupervisingActor], "supervising-actor")
    supervisingActor ! "failChild"
  }



}